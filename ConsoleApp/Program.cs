﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Converters;


namespace ConsoleApp
{
    class Program
    {

        private static JavaScriptSerializer jss = new JavaScriptSerializer();
        static void Main(string[] args)
        {
            int n1 = 10;
            Dictionary<object, object> dyn = new Dictionary<object, object>();

            try
            {
                dyn.Add("111", DateTime.Now);

                dyn.Add("222", DateTime.Now);

                int res = n1 / int.Parse("0");
                
                
            }
            catch (Exception ex)
            {
                Utils.LogUtils.LogError("运算出错", Utils.Developer.XiaoQiang, ex, n1, dyn);
            }

            string line = File.ReadAllText(Path.GetFullPath("log/" + DateTime.Now.ToString("yyyyMMdd") + ".log"), Encoding.UTF8)
                .Split(new string[] { "----------------------------------------------------------------" }, StringSplitOptions.None)[0];


            //时间格式化
            IsoDateTimeConverter dtConverter = new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-dd HH:mm:ss:fff" };
            //枚举类型格式化
            StringEnumConverter emConverter = new StringEnumConverter();

            List<Newtonsoft.Json.JsonConverter> lstJCvtr = new List<Newtonsoft.Json.JsonConverter>();
            lstJCvtr.Add(dtConverter);
            lstJCvtr.Add(emConverter);


            Log log = Newtonsoft.Json.JsonConvert.DeserializeObject<Log>(line, lstJCvtr.ToArray());
        }
    }
}
